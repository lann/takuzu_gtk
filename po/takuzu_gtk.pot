# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-29 18:18+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/dlg_new_takuzu.cpp:12
msgid "Cancel"
msgstr ""

#: src/Takuzu.cpp:293
msgid "Cannot save the grid onto the csv file"
msgstr ""

#: src/dlg_random_takuzu.cpp:8
msgid "Create grid progress"
msgstr ""

#: src/Takuzu.cpp:190
msgid "Created by"
msgstr ""

#: src/Takuzu.cpp:178
msgid "Error in the Takuzu Grid"
msgstr ""

#: src/Takuzu.cpp:13
msgid "File"
msgstr ""

#: src/Takuzu.cpp:261 src/Takuzu.cpp:296
msgid "File validity"
msgstr ""

#: src/dlg_new_takuzu.cpp:4
msgid "Height"
msgstr ""

#: src/Takuzu.cpp:13
msgid "Help"
msgstr ""

#: src/dlg_new_takuzu.cpp:11
msgid "OK"
msgstr ""

#: src/Takuzu.cpp:234
msgid "Open a Takazu csv file"
msgstr ""

#: src/Takuzu.cpp:13
msgid "Random grid"
msgstr ""

#: src/Takuzu.cpp:276
msgid "Save a Takazu csv file"
msgstr ""

#: src/Takuzu.cpp:13
msgid "Solve"
msgstr ""

#: src/Takuzu.cpp:191
msgid ""
"Takuzu is a program to solve Takuzu grid\n"
"\n"
"For solve a Takazu problem, you must respect these rules : \n"
"- Each row and each column must contain an equal number of 0s and 1s\n"
"- More than two of the same digits can't be adjacent.\n"
"- Each row and column is unique"
msgstr ""

#: src/Takuzu.cpp:258
msgid "The csv file is not valid"
msgstr ""

#: src/Takuzu.cpp:177
msgid "The number is not possible. You have an error in your Takuzu grid"
msgstr ""

#: src/dlg_new_takuzu.cpp:4
msgid "Width"
msgstr ""

#: src/Takuzu.cpp:239 src/Takuzu.cpp:281
msgid "csv files"
msgstr ""
