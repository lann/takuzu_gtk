#include "dlg_new_takuzu.h"
#ifndef DLG_RANDOM_TAKUZU_H
#define DLG_RANDOM_TAKUZU_H
class Dlg_Random_Takuzu : public Dlg_New_Takuzu
{
public:
     Dlg_Random_Takuzu();
     ~Dlg_Random_Takuzu();
     void Set_fraction_progress_bar(double fraction);
private:
     Gtk::ProgressBar m_progress_bar;
};

#endif
