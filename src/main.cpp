#include <gtkmm/main.h>
#include "Board.h"
#include "Takuzu.h"
#include <iterator>
#include <iostream>
#include <libintl.h>
#include <string>
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif
#define _(String) gettext (String)

int main(int argc, char *argv[])
{
     setlocale (LC_ALL, "");
#ifdef HAVE_CONFIG_H
     {
	  bindtextdomain (PACKAGE, LOCALEDIR);
	  bind_textdomain_codeset(PACKAGE, "UTF-8");
	  textdomain (PACKAGE);
     }
#else
     {
	  bindtextdomain("takuzu_gtk","/usr/share/locale");
	  bind_textdomain_codeset("takuzu_gtk", "UTF-8");
	  textdomain("takuzu_gtk");
     }
#endif
     auto app=Gtk::Application::create(argc, argv);
     Takuzu *pwindow=0;
     pwindow=new Takuzu;
     return app->run(*pwindow);
}
