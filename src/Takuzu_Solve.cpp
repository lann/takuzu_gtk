#include "Takuzu_Solve.h"
#include "Board.h"
#include <cstddef>
#include <iostream>
Takuzu_Solve::Takuzu_Solve()
{
  
}

Takuzu_Solve::~Takuzu_Solve()
{

}

Takuzu_Solve::Takuzu_Solve(Board *board)
{
     m_board=board;
}

bool Takuzu_Solve::Try_Populate_Line(size_t row, size_t column, int value, const int direction)
{
     state box_state;
     box_state=m_board->Set_Value(row, column, value);
     if (box_state.validity)
     {
	  size_t last_column(column+3*direction);
	  if ((last_column<m_board->Get_size_column()) && (last_column>=0))
	  {
	       box_state=m_board->Set_Value(row, last_column, value);
	  }
     }
     return box_state.validity;
}

bool Takuzu_Solve::Try_Populate_Column(size_t row, size_t column, int value, const int direction)
{
     state box_state;
     box_state=m_board->Set_Value(row, column, value);
     if (box_state.validity)
     {
	  size_t last_line(row+3*direction);
	  if ((last_line<m_board->Get_size_row()) && (last_line>=0))
	  {
	       box_state=m_board->Set_Value(last_line, column, value);
	  }
     }
     return box_state.validity;
}

bool Takuzu_Solve::Solve()
{
     bool finished(false);
     state box_state;
     std::string out;
     box_state.validity=true;
     while ((!finished) && (box_state.validity))
     {
	  finished=true;
	  for (size_t i=0; i<m_board->Get_size_row() && box_state.validity;i++)
	  {
	       for (size_t j=0; j<m_board->Get_size_column() && box_state.validity;j++)
	       {
		    for (size_t k=0;k<2 && box_state.validity;k++)
		    {
			 int return_find;
			 if (m_board->Get_Value(i, j)==-1)
			 {
			      return_find=Find_double_line(i, j, k);
			      if (return_find!=false)
			      {
				   box_state.validity=Try_Populate_Line(i, j, !k, return_find);
				   finished=!box_state.validity;
			      }
			      return_find=Find_double_column(i, j, k);
			      if ((return_find!=false) && (box_state.validity))
			      {
				   box_state.validity=Try_Populate_Column(i, j, !k, return_find);
				   finished=!box_state.validity;
			      }
			      if (((Find_side_line(i, j, k)) || (Find_side_column(i, j, k))) && (box_state.validity))
			      {
				   box_state=m_board->Set_Value(i, j, !k);
				   finished=!box_state.validity;
			      }
			      if (Try_complete_line(i, k) && (box_state.validity))
			      {
				   for(size_t c=0;c<m_board->Get_size_column() && box_state.validity;c++)
				   {
					box_state=m_board->Set_Value(i, c, k);
					finished=!box_state.validity;
				   }
			      }
			      if (Try_complete_column(j, k) && (box_state.validity))
			      {
				   for(size_t r=0;r<m_board->Get_size_row() && box_state.validity;r++)
				   {
					box_state=m_board->Set_Value(r, j, k);
					finished=!box_state.validity;
				   }
			      }
			 }
		    }
	       }
	  }
     }
     return box_state.validity;
}

void Takuzu_Solve::Simplify()
{
     for(size_t i=0;i< m_board->Get_size_row();i++)
     {
	  for (size_t j=0;j<m_board->Get_size_column();j++)
	  {
	       int value=m_board->Get_Value(i, j);
	       if (value>=0)
	       {
		    if ((Find_double_line(i, j, !value)!=0) || (Find_double_column(i, j, !value)!=0) || (Find_side_line(i, j, !value)) || (Find_side_column(i, j, !value)))
		    {
			 Board *tab_simplify=new Board;
			 *tab_simplify=*m_board;
			 tab_simplify->Set_Value(i, j, -1);
			 Takuzu_Solve takuzu_solve_simplify(tab_simplify);
			 if (takuzu_solve_simplify.Solve())
			 {
			      m_board->Set_Value(i, j, -1);
			 }
			 delete tab_simplify;
		    }
	       }
	  }
     }
}

int Takuzu_Solve::Find_double_line(size_t row, size_t column, int value)
{
     if ((m_board->Get_Value(row, column+1)==value) && (m_board->Get_Value(row, column+2)==value))
     {
	  return n_right_or_down_direction;
     }
     else if ((m_board->Get_Value(row, column-1)==value) && (m_board->Get_Value(row, column-2)==value))
     {
	  return n_left_or_up_direction;
     }
     else
     {
	  return false;
     }
}

int Takuzu_Solve::Find_double_column(size_t row, size_t column, int value)
{
     if ((m_board->Get_Value(row+1, column)==value) && (m_board->Get_Value(row+2, column)==value))
     {
	  return n_right_or_down_direction;
     }
     else if ((m_board->Get_Value(row-1, column)==value) && (m_board->Get_Value(row-2, column)==value))
     {
	  return n_left_or_up_direction;
     }
     else
     {
	  return false;
     }
}

bool Takuzu_Solve::Find_side_line(size_t row, size_t column, int value)
{
     if ((m_board->Get_Value(row+1, column)==value) && (m_board->Get_Value(row-1, column)==value))
     {
	  return true;
     }
     else
     {
	  return false;
     }
}

bool Takuzu_Solve::Find_side_column(size_t row, size_t column, int value)
{
     if ((m_board->Get_Value(row, column+1)==value) && (m_board->Get_Value(row, column-1)==value))
     {
	  return true;
     }
     else
     {
	  return false;
     }
}

bool Takuzu_Solve::Try_complete_line(size_t row, int value)
{
     bool ret=false;
     size_t quantity_inv_value(0), quantity_value(0);

     for (size_t i=0; i<m_board->Get_size_column();i++)
     {
	  if (m_board->Get_Value(row, i)==!value)
	  {
	       quantity_inv_value++;
	  }
	  if (m_board->Get_Value(row, i)==value)
	  {
	       quantity_value++;
	  }
     }
     if ((quantity_inv_value==m_board->Get_size_column()/2) && (quantity_value!=m_board->Get_size_column()/2))
     {
	  ret=true;
     }
     return ret;
}

bool Takuzu_Solve::Try_complete_column(size_t column,int value)
{
     bool ret=false;
     size_t quantity_inv_value(0), quantity_value(0);

     for (size_t i=0; i<m_board->Get_size_row();i++)
     {
	  if (m_board->Get_Value(i, column)==!value)
	  {
	       quantity_inv_value++;
	  }
	  if (m_board->Get_Value(i, column)==value)
	  {
	       quantity_value++;
	  }
     }
     if ((quantity_inv_value==m_board->Get_size_row()/2) && (quantity_value!=m_board->Get_size_row()/2))
     {
	  ret=true;
     }
     return ret;
}
