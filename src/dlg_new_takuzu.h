#include <gtkmm.h>
#ifndef DLG_NEW_TAKUZU_H
#define DLG_NEW_TAKUZU_H
class Dlg_New_Takuzu : public Gtk::Dialog
{
public:
     Dlg_New_Takuzu();
     ~Dlg_New_Takuzu();
     unsigned int Get_Height();
     unsigned int Get_Width();
protected:
     virtual void OnCancel();
     virtual void entry_changed(void);
private:
     unsigned int m_Height, m_Width;
     Gtk::HBox Hbox_label, Hbox_entry;
     Gtk::Label lHeight, lWidth;
     Gtk::Entry eHeight, eWidth;
     Gtk::Button  *bCancel, *bOK;
};
#endif
