#ifndef BOARD_H_INCLUDED
#define BOARD_H_INCLUDED
#include <cstddef>
#include <fstream>
#include <string>
#include <vector>

typedef struct
{
     bool validity;
     bool empty;
}state;

class Board
{
public:
     Board();
     Board(size_t n_row, size_t n_col);
     ~Board();
     void Create();
     bool Create_from_file(std::string file);
     bool Save(std::string file);
     bool Validity_Value(size_t row, size_t column, int value);
     state Set_Value(size_t row, size_t column, int value);
     int Get_Value(size_t row, size_t column) const;
     size_t Get_size_row(void) const;
     size_t Get_size_column(void) const;
     void Set_size_row(size_t row);
     void Set_size_column(size_t column);
     bool Is_Complete(void);
     Board & operator=(const Board &);
     size_t Get_cases_in(void);
     void Clean(void);
     void Display(void);
private:
     size_t Search_Columns(std::string line);
     bool Validity_Column(size_t column);
     bool Validity_Row(size_t row);
     size_t m_row,m_column;
     std::vector<std::vector<int> > m_vecboard;
     std::fstream file_csv;
};
#endif
