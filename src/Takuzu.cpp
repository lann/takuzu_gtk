#include "Takuzu.h"
#include "Takuzu_Solve.h"
#include <cstddef>
#include <libintl.h>
#include <gdkmm.h>
#include <iostream>
#include <random>
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#define _(String) gettext (String)

Takuzu::Takuzu() : m_menuitem_file(_("File")), m_menuitem_takuzu("Takuzu"), m_menuitem_help(_("Help")), m_menu_image_quit(Gtk::StockID("gtk-quit")), m_menu_image_new(Gtk::StockID("gtk-new")), m_menu_image_about(Gtk::StockID("gtk-about")), m_menu_image_open(Gtk::StockID("gtk-open")), m_menu_image_save(Gtk::StockID("gtk-save")), m_menu_image_solve(_("Solve")), m_menu_image_random(_("Random grid"))
{
#ifdef HAVE_CONFIG_H
     m_sz_share=GLADE_DIR;
#endif
     if (m_sz_share!="")
     {
	  std::string szicon(m_sz_share+"/Takuzu.jpg");
	  set_icon(Gdk::Pixbuf::create_from_file(szicon));
	  m_pic_solve.set(m_sz_share+"/solve.png");
	  m_pic_random.set(m_sz_share+"/random.jpg");
	  m_menu_image_solve.set_image(m_pic_solve);
	  m_menu_image_random.set_image(m_pic_random);
	  dialog_new_takuzu.set_icon_from_file(m_sz_share+"/Takuzu.jpg");
	  dialog_random_takuzu.set_icon_from_file(m_sz_share+"/Takuzu.jpg");
     }
     tab=nullptr;
     tab_solve=nullptr;
     m_dlg_open=nullptr;
     m_grid=new Gtk::Grid;
     m_menuitem_file.set_submenu(m_menu_file);
     m_menuitem_takuzu.set_submenu(m_menu_takuzu);
     m_menuitem_help.set_submenu(m_menu_help);
     m_menu_file.append(m_menu_image_new);
     m_menu_file.append(m_menu_image_open);
     m_menu_file.append(m_menu_image_save);
     m_menu_file.append(m_menu_image_quit);
     m_menu_takuzu.append(m_menu_image_solve);
     m_menu_takuzu.append(m_menu_image_random);
     m_menu_help.append(m_menu_image_about);
     m_menu_image_quit.signal_activate().connect(sigc::mem_fun(*this,&Takuzu::on_Quit));
     m_menu_image_open.signal_activate().connect(sigc::mem_fun(*this,&Takuzu::on_Open));
     m_menu_image_save.signal_draw().connect(sigc::mem_fun(*this, &Takuzu::on_Draw_Menu_Solve));
     m_menu_image_save.signal_activate().connect(sigc::mem_fun(*this,&Takuzu::on_Save));
     m_menu_image_new.signal_activate().connect(sigc::mem_fun(*this,&Takuzu::on_New));
     m_menu_image_solve.signal_activate().connect(sigc::mem_fun(*this,&Takuzu::on_Solve));
     m_menu_image_solve.signal_draw().connect(sigc::mem_fun(*this,&Takuzu::on_Draw_Menu_Solve));
     m_menu_image_random.signal_activate().connect(sigc::mem_fun(*this,&Takuzu::on_Random));
     m_menu_image_about.signal_activate().connect(sigc::mem_fun(*this,&Takuzu::on_Dialog_About));
     signal_hide().connect(sigc::mem_fun(*this,&Takuzu::on_Hide));
     m_menubar.append(m_menuitem_file);
     m_menubar.append(m_menuitem_takuzu);
     m_menubar.append(m_menuitem_help);
     VBox1.pack_start(m_menubar,Gtk::PACK_SHRINK);
     VBox1.pack_start(*m_grid,Gtk::PACK_SHRINK);
     add(VBox1);
     set_default_size(200, 100);
     show_all_children();
}

Takuzu::~Takuzu()
{
     Delete_entry_tab();
     if (m_grid!=nullptr)
     {
	  delete m_grid;
     }
     if (m_dlg_open!=nullptr)
     {
	  delete m_dlg_open;
     }
}

void Takuzu::on_Hide(void)
{
     on_Quit();
}

void Takuzu::on_Quit()
{
     dialog_random_takuzu.hide();
     hide();
     close();
}

void Takuzu::Delete_entry_tab(void)
{
     if (m_grid!=nullptr && tab!=nullptr)
     {
	  for (size_t i=0;i<tab->Get_size_row();i++)
	  {
	       for (size_t j=0; j<tab->Get_size_column();j++)
	       {
		    delete m_grid->get_child_at(j, i);
	       }
	  }
     }
     if (tab!=nullptr)
     {
	  delete tab;
	  tab=nullptr;
     }
     if (tab_solve!=nullptr)
     {
	  delete tab_solve;
	  tab_solve=nullptr;
     }
}

void Takuzu::on_New()
{
     int answer;
     answer=dialog_new_takuzu.run();
     if (answer==Gtk::RESPONSE_OK)
     {
	  New_Tab(dialog_new_takuzu.Get_Height(), dialog_new_takuzu.Get_Width());
     }
     dialog_new_takuzu.hide();
}

void Takuzu::New_Tab(size_t row, size_t column)
{
     Delete_entry_tab();
     tab=new Board(row, column);
     Create_Grid();
}

void Takuzu::Create_Grid(void)
{
     if (tab!=nullptr)
     {
	  for (size_t i=0; i<tab->Get_size_row(); i++)
	  {
	       for (size_t j = 0; j < tab->Get_size_column(); j++) {
		    entry = new Gtk::Entry;
		    entry->set_max_length(1);
		    entry->set_width_chars(1);
		    entry->signal_changed().connect(
			 sigc::bind(sigc::mem_fun(*this, &Takuzu::on_entry_changed), i, j));
		    m_grid->attach(*entry, j, i);
	       }
	  }
	  resize(200,100);	  
	  show_all_children();
     }
}

void Takuzu::on_entry_changed(size_t row, size_t column)
{
     std::string szentry;
     Gtk::Entry *entry;
     entry=(Gtk::Entry*)m_grid->get_child_at(column, row);
     if (entry!=nullptr)
     {
	  szentry=entry->get_text();
	  if ((szentry!="0" ) && (szentry!="1"))
	  {
	       entry->set_text("");
	       tab->Set_Value(row, column, -1);
	  }
	  else
	  {
	       std::istringstream istr;
	       int val;
	       istr.str(szentry);
	       istr>>val;
	       if (tab->Validity_Value(row, column, val))
	       {
		    tab->Set_Value(row, column, val);
	       }
	       else
	       {
		    entry->set_text("");
		    tab->Set_Value(row, column, -1);
		    Gtk::MessageDialog Dialog(_("The number is not possible. You have an error in your Takuzu grid"), false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
		    Dialog.set_title(_("Error in the Takuzu Grid"));
		    Dialog.run();
		    Dialog.hide();	    
	       }
	  }
     }
}

void Takuzu::on_Dialog_About()
{
     std::vector<Glib::ustring> people;
     people.push_back("lann");
     m_dlgAbout.add_credit_section(_("Created by"), people);
     m_dlgAbout.set_comments(_("Takuzu is a program to solve Takuzu grid\n\nFor solve a Takazu problem, you must respect these rules : \n- Each row and each column must contain an equal number of 0s and 1s\n- More than two of the same digits can't be adjacent.\n- Each row and column is unique"));
     m_dlgAbout.set_license_type(Gtk::License::LICENSE_GPL_3_0);
     m_dlgAbout.set_program_name("Takuzu");
     m_dlgAbout.set_version("0.4.2.2");
     m_dlgAbout.set_website("https://numeriquement.fr");
     m_dlgAbout.set_translator_credits("lann");
     if (m_sz_share!="")
     {
	  m_dlgAbout.set_logo(Gdk::Pixbuf::create_from_file(m_sz_share+"/Takuzu.jpg"));
     }
     m_dlgAbout.set_icon(get_icon());
     m_dlgAbout.run();
     m_dlgAbout.hide();
}

void Takuzu::Display(void)
{
     if (tab!=nullptr)
     {
	  for (size_t i=0; i<tab->Get_size_row();i++)
	  {
	       for(size_t j=0; j<tab->Get_size_column();j++)
	       {
		    Gtk::Entry *entry;
		    entry=(Gtk::Entry*)m_grid->get_child_at(j,i);
		    if (tab->Get_Value(i, j)==0)
		    {
			 entry->set_text("0");
		    }
		    else if (tab->Get_Value(i, j)==1)
		    {
			 entry->set_text("1");
		    }
	       }
	  }
     }
}
	  
void Takuzu::on_Open(void)
{
     int result(0);
     if (m_dlg_open==nullptr)
     {
	  m_dlg_open=new Gtk::FileChooserDialog(_("Open a Takazu csv file"), Gtk::FILE_CHOOSER_ACTION_OPEN);
	  m_dlg_open->set_transient_for(*this);
	  m_dlg_open->add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	  m_dlg_open->add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);
	  auto filter_csv = Gtk::FileFilter::create();
	  filter_csv->set_name(_("csv files"));
	  filter_csv->add_pattern("*.csv");
	  m_dlg_open->add_filter(filter_csv);
     }
     if (m_dlg_open!=nullptr)
     {
	  result=m_dlg_open->run();
	  m_dlg_open->hide();
	  if ((result==Gtk::RESPONSE_OK) && (m_dlg_open->get_filename()!=""))
	  {
	       Delete_entry_tab();
	       tab=new Board;
	       if (tab->Create_from_file(m_dlg_open->get_filename()))
	       {
		    Create_Grid();
		    Display();
	       }
	       else
	       {
		    Gtk::MessageDialog Dialog(_("The csv file is not valid"), false,
					      Gtk::MESSAGE_ERROR, Gtk::BUTTONS_CLOSE,
					      false);
		    Dialog.set_title(_("File validity"));
		    Dialog.run();
		    Dialog.hide();
	       }
	  }
	  delete m_dlg_open;
	  m_dlg_open=nullptr;
     }
}

void Takuzu::on_Save(void)
{
     int result(0);
     if (m_dlg_open==nullptr)
     {
	  m_dlg_open=new Gtk::FileChooserDialog(_("Save a Takazu csv file"), Gtk::FILE_CHOOSER_ACTION_SAVE);
	  m_dlg_open->set_transient_for(*this);
	  m_dlg_open->add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	  m_dlg_open->add_button(Gtk::Stock::SAVE, Gtk::RESPONSE_OK);
	  auto filter_csv = Gtk::FileFilter::create();
	  filter_csv->set_name(_("csv files"));
	  filter_csv->add_pattern("*.csv");
	  m_dlg_open->add_filter(filter_csv);
     }
     if (m_dlg_open!=nullptr)
     {
	  result=m_dlg_open->run();
	  m_dlg_open->hide();
	  if ((result==Gtk::RESPONSE_OK) && (m_dlg_open->get_filename()!=""))
	  {
	       if (!tab->Save(m_dlg_open->get_filename()))
	       {
		    Gtk::MessageDialog Dialog(_("Cannot save the grid onto the csv file"), false,
					      Gtk::MESSAGE_ERROR, Gtk::BUTTONS_CLOSE,
					      false);
		    Dialog.set_title(_("File validity"));
		    Dialog.run();
		    Dialog.hide();
	       }
	  }
	  delete m_dlg_open;
	  m_dlg_open=nullptr;
     }
}

void Takuzu::on_Solve(void)
{
     Takuzu_Solve takuzu_solve(tab);
     takuzu_solve.Solve();
     Display();     
}

bool Takuzu::on_Draw_Menu_Solve(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
     if (tab!=nullptr)
     {
	  m_menu_image_solve.set_sensitive(true);
	  m_menu_image_save.set_sensitive(true);
     }
     else
     {
	  m_menu_image_solve.set_sensitive(false);
	  m_menu_image_save.set_sensitive(false);
     }
     return true;
}

void Takuzu::on_Random(void)
{
     int answer;
     answer=dialog_random_takuzu.run();
     if (answer==Gtk::RESPONSE_OK)
     {
	  New_Tab(dialog_random_takuzu.Get_Height(), dialog_random_takuzu.Get_Width());
	  if (tab_solve==nullptr)
	  {
	       tab_solve=new Board;
	  }
	  Takuzu_Solve takuzu_solve(tab_solve);
	  std::random_device rd;
	  std::mt19937 mt(rd());
	  std::uniform_int_distribution<size_t> dist_row(0, tab->Get_size_row()-1), dist_col(0, tab->Get_size_column()-1), dist_value(0,1);
	  *tab_solve=*tab;
	  while ((!tab_solve->Is_Complete()) && (dialog_random_takuzu.is_visible()))
	  {
	       size_t row_rnd, column_rnd,value_rnd;
	       state box_state;
	       row_rnd=dist_row(mt);
	       column_rnd=dist_col(mt);
	       value_rnd=dist_value(mt);
	       box_state=tab_solve->Set_Value(row_rnd, column_rnd, value_rnd);
	       if ((box_state.empty) && (box_state.validity))
	       {
		    if (takuzu_solve.Solve())
		    {
			 tab->Set_Value(row_rnd, column_rnd, value_rnd);
			 dialog_random_takuzu.Set_fraction_progress_bar((double)((double)tab_solve->Get_cases_in()/((double)tab_solve->Get_size_column()*(double)tab_solve->Get_size_row())));
		    }
		    else
		    {
			 Clean_Tabs();
		    }
	       }
	  }
     }
     if (!dialog_random_takuzu.is_visible())
     {
	  Clean_Tabs();
     }
     else
     {
	  Takuzu_Solve takuzu_simplify(tab);
	  takuzu_simplify.Simplify();
	  dialog_random_takuzu.hide();
     }
     Display(); 
}

void Takuzu::Clean_Tabs(void)
{
     if (tab!=nullptr)
     {
	  tab->Clean();
     }
     if (tab_solve!=nullptr)
     {
	  tab_solve->Clean();
     }
}

