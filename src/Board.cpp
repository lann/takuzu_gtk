#include "Board.h"
#include <cstddef>
#include <ios>
#include <iostream>
#include <libintl.h>
#include <string>
#define _(String) gettext (String)
Board::Board()
{
  
}

Board::Board(size_t n_row, size_t n_col)
{
     m_row=n_row;
     m_column=n_col;
     Create();
}

Board::~Board() {}

void Board::Create(void)
{
     std::vector<int> vec(m_column,-1);
     for (size_t i=0; i<m_row;i++)
     {
	  m_vecboard.push_back(vec);
     }
}

bool Board::Save(std::string file)
{
     bool ret=true;
     file_csv.open(file,std::ios_base::out);
     if (file_csv.is_open())
     {
	  for(size_t i=0; i<m_row;i++)
	  {
	       for(size_t j=0; j<m_column;j++)
	       {
		    file_csv<<Get_Value(i, j)<<";";
	       }
	       if (i<m_row-1)
	       {
		    file_csv<<std::endl;
	       }
	  }
     }
     else
     {
	  ret=false;
     }
     file_csv.close();
     return ret;
}

bool Board::Create_from_file(std::string file)
{
     bool ret=true;
     state box_state;
     file_csv.open(file,std::ios_base::in);
     if (file_csv.is_open())
     {
	  size_t lines(0),columns(0);
	  std::vector<std::string> vec_lines;
	  while (!file_csv.eof())
	  {
	       std::string line;
	       std::getline(file_csv,line);
	       vec_lines.push_back(line);
	       lines++;
	  }
	  if (vec_lines.size()>0)
	  {
	       for (size_t j=0; ((j<vec_lines.size()) && (ret==true)) ;j++)
	       {
		    size_t column(0);
		    std::string line_1;
		    column=Search_Columns(vec_lines.at(j));
		    if (j==0)
		    {
			 columns=column;
		    }
		    else if ((j!=0) && (column!=columns))
		    {
			 ret=false;
		    }
	       }
	  }
	  if (ret==true)
	  {
	       if ((columns%2!=0) || (lines%2!=0))
	       {
		    ret=false;
	       }
	       else
	       {
		    std::string line, caracter;
		    m_column=columns;
		    m_row=lines;
		    Create();
		    for (size_t i=0; i<vec_lines.size() && ret;i++)
		    {
			 size_t number_column(0);
			 line=vec_lines.at(i);
			 for (size_t j=0; j<line.size() && ret;j++)
			 {
			      caracter=line.at(j);
			      if (caracter!=";")
			      {
				   if (caracter=="0")
				   {
					box_state=Set_Value(i, number_column, 0);
					ret=box_state.validity;
				   }
				   else if (j!=0)
				   {
					std::string carac_minus;
					carac_minus=line.at(j-1);
					if ((carac_minus!="-") && (caracter=="1"))
					{
					     box_state=Set_Value(i, number_column, 1);
					     ret=box_state.validity;
					}
				   }
				   else if ((j==0) && (caracter=="1"))
				   {
					box_state=Set_Value(i, number_column, 1);
					ret=box_state.validity;
				   }
			      }
			      else
			      {
				   number_column++;
			      }
			 }
		    }
	       }
	  }
     }
     else
     {
	  ret=false;
     }
     file_csv.close();
     return ret;
}

size_t Board::Search_Columns(std::string line)
{
     size_t ret(0);
     for(size_t i=0;i<line.size();i++)
     {
	  std::string carac;
	  carac=line.at(i);
	  if (carac==";")
	  {
	       ret++;
	  }
     }
     return ret;
}

bool Board::Validity_Value(size_t row, size_t column, int value)
{
     bool ret=true;
     if ((row>=0) && (row<m_row) && (column>=0) && (column<m_column))
     {
	  m_vecboard.at(row).at(column)=value;
	  size_t number_element=0;
	  int test_element=-1;
	  
	  for (size_t i=0;i<m_column && ret;i++)
	  {
	       if (test_element==-1)
	       {
		    test_element=m_vecboard.at(row).at(i);
		    number_element=1;
	       }
	       else if (test_element==m_vecboard.at(row).at(i))
	       {
		    number_element++;
	       }
	       else
	       {
		    test_element=m_vecboard.at(row).at(i);
		    number_element=1;
	       }
	       if (number_element>2)
	       {
		    ret=false;
	       }	       
	  }
	  if (ret)
	  {
	       test_element=-1;
	       number_element=0;
	       for (size_t i=0;i<m_row && ret;i++)
	       {
		    if (test_element==-1)
		    {
			 test_element=m_vecboard.at(i).at(column);
			 number_element=1;
		    }
		    else if (test_element==m_vecboard.at(i).at(column))
		    {
			 number_element++;
		    }
		    else
		    {
			 test_element=m_vecboard.at(i).at(column);
			 number_element=1;
		    }
		    if (number_element>2)
		    {
			 ret=false;
		    }
	       }
	       if (ret)
	       {
		    ret=Validity_Column(column);
		    if (ret)
		    {
			 ret=Validity_Row(row);
		    }
	       }
	  }
	  if (!ret)
	  {
	       m_vecboard.at(row).at(column)=-1;
	  }
     }
     else
     {
	  ret=false;
     }
     return ret;
}

bool Board::Validity_Column(size_t column)
{
     bool ret=true;
     size_t zero(0), one(0);
     for (size_t i=0; i<m_row; i++)
     {
	  if (m_vecboard.at(i).at(column)==0)
	  {
	       zero++;
	  }
	  else if (m_vecboard.at(i).at(column)==1)
	  {
	       one ++;
	  }
     }
     if ((zero>m_row/2) || (one>m_row/2))
     {
	  ret=false;
     }
     return ret;
}

bool Board::Validity_Row(size_t row)
{
     bool ret=true;
     size_t zero(0), one(0);
     for (size_t i=0; i<m_column; i++)
     {
	  if (m_vecboard.at(row).at(i)==0)
	  {
	       zero++;
	  }
	  else if (m_vecboard.at(row).at(i)==1)
	  {
	       one ++;
	  }
     }
     if ((zero>m_column/2) || (one>m_column/2))
     {
	  ret=false;
     }
     return ret;
}

state Board::Set_Value(size_t row, size_t column, int value)
{
     state ret;
     ret.empty=true;
     ret.validity=true;
     if ((row<m_row) && (column<m_column))
     {
	  if ((m_vecboard.at(row).at(column)==-1) || (m_vecboard.at(row).at(column)==value))
	  {
	       if (Validity_Value(row, column, value))
	       {
		    m_vecboard.at(row).at(column)=value;
		    ret.empty=true;
		    ret.validity=true;
	       }
	       else
	       {
		    ret.empty=true;
		    ret.validity=false;
	       }
	  }
	  else if (value==-1)
	  {
	       m_vecboard.at(row).at(column)=-1;
	       ret.empty=true;
	       ret.validity=true;
	  }
	  else
	  {
	       ret.validity=true;
	       ret.empty=false;
	  }
     }
     else
     {
	  ret.empty=false;
	  ret.validity=false;
     }
     return ret;
}

int Board::Get_Value(size_t row, size_t column) const
{
     if ((row<m_row) && (column<m_column))
     {
	  return m_vecboard.at(row).at(column);
     }
     else
     {
	  return -2;
     }
}

size_t Board::Get_size_column() const
{
     return m_column;
}

size_t Board::Get_size_row() const
{
     return m_row;
}

void Board::Set_size_row(size_t row)
{
     m_row=row;
}

void Board::Set_size_column(size_t column)
{
     m_column = column;
}

bool Board::Is_Complete(void)
{
     bool ret(true);
     for(size_t i=0; i<Get_size_row() && ret;i++)
     {
	  for(size_t j=0; j<Get_size_column() && ret; j++)
	  {
	       if (Get_Value(i, j)==-1)
	       {
		    ret=false;
	       }
	  }
     }
     return ret;
}

Board& Board::operator=(const Board &tab)
{
     m_row=tab.Get_size_row();
     m_column=tab.Get_size_column();
     Create();
     for (size_t i=0; i<tab.Get_size_row(); i++)
     {
	  for (size_t j=0; j<tab.Get_size_column();j++)
	  {
	       m_vecboard.at(i).at(j)=tab.Get_Value(i, j);
	  }
     }
     return *this;
}

size_t Board::Get_cases_in()
{
     size_t case_in(0);
     for(size_t i=0;i<m_row;i++)
     {
	  for (size_t j=0;j<m_column;j++)
	  {
	       if (Get_Value(i, j)!=-1)
	       {
		    case_in++;
	       }
	  }
     }
     return case_in;
}

void Board::Clean()
{
     for (size_t i=0; i<m_row;i++)
     {
	  for (size_t j=0; j<m_column;j++)
	  {
	       m_vecboard.at(i).at(j)=-1;
	  }
     }
}

void Board::Display()
{
     std::cout<<std::endl;
     for(size_t i=0;i<m_row;i++)
     {
	  std::cout<<"|";
	  for(size_t j=0;j<m_column;j++)
	  {
	       if (m_vecboard.at(i).at(j)!=-1)
	       {
		    std::cout<<m_vecboard.at(i).at(j)<<"|";
	       }
	       else
	       {
		    std::cout<<" |";
	       }
	  }
	  std::cout<<std::endl;
     }
}
