#include "dlg_random_takuzu.h"
#include <libintl.h>
#define _(String) gettext (String)
Dlg_Random_Takuzu::Dlg_Random_Takuzu()
{
     m_progress_bar.set_fraction(0.0);
     m_progress_bar.set_show_text();
     m_progress_bar.set_text(_("Create grid progress"));
     get_content_area()->pack_start(m_progress_bar, Gtk::PACK_SHRINK);
     show_all_children();
}

Dlg_Random_Takuzu::~Dlg_Random_Takuzu()
{

}

void Dlg_Random_Takuzu::Set_fraction_progress_bar(double fraction)
{
     m_progress_bar.set_fraction(fraction);
     while ( Gtk::Main::events_pending() ) Gtk::Main::iteration();
     show_all_children();
}
