#include <gtkmm.h>
#include <string>
#include "dlg_new_takuzu.h"
#include "dlg_random_takuzu.h"
#include "Board.h"
#include "gtkmm/image.h"
#ifndef TAKUZU_H_INCLUDED
#define TAKUZU_H_INCLUDED
class Takuzu : public Gtk::Window
 {
 public:
      Takuzu();
      ~Takuzu();
 protected:
      virtual void on_Hide(void);
      virtual void on_Quit();
      virtual void on_New();
      virtual void on_Open(void);
      virtual void on_Save(void);
      virtual void on_Solve(void);
      virtual void on_Random(void);
      virtual bool on_Draw_Menu_Solve(const ::Cairo::RefPtr< ::Cairo::Context>& cr);
      virtual void on_Dialog_About(void);
 private:
      void Delete_entry_tab(void);
      void on_entry_changed(size_t row, size_t column);
      void Display(void);
      void Create_Grid(void);
      void Clean_Tabs(void);
      void New_Tab(size_t row, size_t column);
      Gtk::Grid *m_grid;
      Gtk::Image m_pic_solve, m_pic_random;
      Gtk::ComboBoxText *m_combo_text;
      Gtk::MenuBar m_menubar;
      Gtk::Menu m_menu_file, m_menu_help, m_menu_takuzu;
      Gtk::MenuItem m_menuitem_file, m_menuitem_help, m_menuitem_takuzu;
      Gtk::ImageMenuItem m_menu_image_quit, m_menu_image_new, m_menu_image_open, m_menu_image_save, m_menu_image_about, m_menu_image_solve, m_menu_image_random;
      Gtk::VBox VBox1;
      Gtk::FileChooserDialog *m_dlg_open;
      Dlg_New_Takuzu dialog_new_takuzu;
      Dlg_Random_Takuzu dialog_random_takuzu;
      Board *tab, *tab_solve;
      Gtk::Entry *entry;
      Gtk::AboutDialog m_dlgAbout;
      std::string m_sz_share;
};
#endif
