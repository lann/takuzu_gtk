#include "dlg_new_takuzu.h"
#include <libintl.h>
#define _(String) gettext (String)
Dlg_New_Takuzu::Dlg_New_Takuzu() : lHeight(_("Height")), lWidth(_("Width"))
{
     m_Height=0;
     m_Width=0;
     Hbox_label.pack_start(lHeight);
     Hbox_label.pack_start(lWidth);
     get_content_area()->set_homogeneous();
     bOK=add_button(_("OK"), Gtk::RESPONSE_OK);
     bCancel=add_button(_("Cancel"), Gtk::RESPONSE_CANCEL);
     if (bOK!=nullptr)
     {
	  bOK->set_sensitive(false);
     }
     if (bCancel!=nullptr)
     {
	  bCancel->signal_clicked().connect(sigc::mem_fun(*this,&Dlg_New_Takuzu::OnCancel));
     }
     Hbox_entry.pack_start(eHeight);
     Hbox_entry.pack_start(eWidth);
     get_content_area()->pack_start(Hbox_label);
     get_content_area()->pack_start(Hbox_entry);
     eHeight.signal_changed().connect(sigc::mem_fun(*this,&Dlg_New_Takuzu::entry_changed));
     eWidth.signal_changed().connect(sigc::mem_fun(*this,&Dlg_New_Takuzu::entry_changed));
     show_all_children();
}

Dlg_New_Takuzu::~Dlg_New_Takuzu() {}

void Dlg_New_Takuzu::OnCancel(void)
{
     hide();
}

void Dlg_New_Takuzu::entry_changed(void)
{
     std::istringstream istr_height, istr_width;

     istr_height.str(eHeight.get_text());
     istr_height>>m_Height;
     istr_width.str(eWidth.get_text());
     istr_width>>m_Width;
     bOK->set_sensitive(false);
     if ((m_Height>1) && (m_Width>1))
     {
	  if ((m_Height%2==0) && (m_Width%2==0))
	  {
	       bOK->set_sensitive();
	  }
     }
}

unsigned int Dlg_New_Takuzu::Get_Height()
{
     return m_Height;
}

unsigned int Dlg_New_Takuzu::Get_Width()
{
     return m_Width;
}
